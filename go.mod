module gitlab.com/goodimpact/goodimpact-hugo/modules/base-structure

go 1.15

require (
	github.com/hankchizljaw/modern-css-reset v0.0.0-20230926104323-b683d8f06c06 // indirect
	github.com/jygastaud/hugo-microtypo/microtypo v0.0.0-20231031142223-35181baaa9fb // indirect
	gitlab.com/goodimpact/every-layout-css v0.0.0-20220819102259-7de9f880570e // indirect
	gitlab.com/goodimpact/every-layout-wc v0.0.0-20210715225714-a9629e5e8f4a // indirect
	gitlab.com/neotericdesign-tools/hugo-content-module-style-guides.git v0.0.0-20200513165827-e43fa80f0593 // indirect
)
