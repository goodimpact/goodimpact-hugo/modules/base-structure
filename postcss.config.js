const autoprefixer = require("autoprefixer");
const presetEnv = require("postcss-preset-env")({
	stage: 0,
	useBuiltIns: "usage",
});
const calc = require("postcss-calc")({});

// TODO: fix PurgeCSS. It needs to look into JS files.
// Check `content` and `extractors` options
//
// content: ["./hugo_stats.json"],
// defaultExtractor: (content) => {
// 	let els = JSON.parse(content).htmlElements;
// 	return els.tags.concat(els.classes, els.ids);
// },
const purgecss = require("@fullhuman/postcss-purgecss")({
	variables: false,
	safelist: {
		standard: [
			"::placeholder",
			"class",
			"href",
			"js-ok",
			/:focus/,
			/tabindex/,
			/aria/,
			/:defined/,
			/wc/,
			/h\d/,
			/data-/,
			/type/, // [type='text'], [type='checkbox'], etc..
		],
	},
});

// TODO: add purgecss (for both prod and dev environments so we don't get surprises…)
module.exports = {
	plugins: [autoprefixer, ...(process.env.HUGO_ENVIRONMENT === "production" ? [presetEnv, calc] : [])],
};
