# Welcome to Good Impact Base Structure for Hugo 👋

![Version](https://img.shields.io/badge/version-0.1-blue.svg?cacheSeconds=2592000)
[![License: AGPLv3](https://img.shields.io/badge/License-AGPLv3-yellow.svg)](https://www.gnu.org/licenses/agpl-3.0.fr.html)
[![Twitter: GoodImpactFR](https://img.shields.io/twitter/follow/GoodImpactFR.svg?style=social)](https://twitter.com/GoodImpactFR)

> Scaffolds a basic HTML / CSS / JS structure for quality HUGO projects

### 🏠 [Homepage](https://gitlab.com/goodimpact/goodimpact-hugo/modules/base-structure)

## Install

Import this module by adding these lines in `config/_default/module.toml`:

```toml
[[imports]]
	path = "gitlab.com/goodimpact/goodimpact-hugo/modules/base-structure"

	[[imports.mounts]]
		source = "assets"
		target = "assets"

	[[imports.mounts]]
		source = "layouts"
		target = "layouts"

	[[imports.mounts]]
		source = "i18n"
		target = "i18n"
```

## Usage

You'll get a basic HTML page out of the box. The main idea is to complete the layout partials to fit your needs.

### Activate web components globally or by page

You can use param 'activate_web_components' in your website pages by either

- adding it to global config (web components will be activated everywhere):

  In `config/_default/params.toml`

  ```toml
  activate_web_components = false
  ```

- or adding it to the specific pages you want web components to be activated:

  Add params in the front matter of your page. Make sure not to forget `_merge = "deep"`

  ```toml
  _merge = "deep"
  activate_web_components = true
  ```

## Author

👤 **Yaacov Cohen**

- Website: https://www.goodimpact.studio
- Twitter: [@GoodImpactFR](https://twitter.com/GoodImpactFR)
- Github: [@yaaax](https://github.com/yaaax)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://gitlab.com/goodimpact/goodimpact-hugo/modules/base-structure/-/issues).

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2021 [Yaacov Cohen](https://github.com/yaaax).

This project is [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.fr.html) licensed.

---

_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
